package com.example.demo.student;

import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "kentaurus/first-project")

public class StudentController {
    @GetMapping
    public List<Student> hello() {
        return List.of(
                new Student(1l, "vivek", 21));
    }
}
